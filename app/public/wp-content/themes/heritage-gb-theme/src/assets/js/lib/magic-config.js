// Scroll Magic Home Sections
var controller = new ScrollMagic.Controller();

var isMobile = {
  iOS: function() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  }
};

if (document.documentMode || /Edge/.test(navigator.userAgent) || (isMobile.iOS)) {
  console.log('this is edge or IE')
}
// else if (iOS == true) {
//   console.log('this is IOS')
// }
else {
  // Attractions Scroll
  $('.attraction-home').each(function () {
    var attractionsHome = new ScrollMagic.Scene({
        triggerHook: 0,
        triggerElement: this,
        //duration: 500,
        //offset: "-50",
        reverse: true,
      })
      .setPin(this, 'attractions-text')
      .setClassToggle(this, 'inview')
    controller.addScene(attractionsHome);
  });


}

// if (document.documentMode || /Edge/.test(navigator.userAgent)) {
//   console.log('this is edge or IE')
// }
// else {
//   console.log('this is not edge or IE')
// }



// Attractions Scroll Image effects
/*
$('.attraction-home').each(function () {
  var tween = TweenMax.to(".attraction-home .image .image-cover", 0.5, {
    filter: opacity(1),
  });
  var attractionsHomeImage = new ScrollMagic.Scene({
      triggerHook: 0,
      triggerElement: this,
      duration: "100%",
      reverse: true,
    })
    .setTween(tween)
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller)
    .setPin(this, 'attractions-text')
  controller.addScene(attractionsHomeImage);
});
*/


// Attractions Menu Effects
var attractionNavigationHome = new ScrollMagic.Scene({
    triggerElement: ('.attraction-home'),
  })
  .setPin('.home .attraction-menu')
  .setClassToggle('.home .attraction-menu', 'inview')

controller.addScene(attractionNavigationHome);

// Listener for side menu
$('.attraction-home').each(function () {
  var attractionID = this.id;
  var attractionsMenu = new ScrollMagic.Scene({
      triggerHook: 'inEnter',
      triggerElement: this,
      duration: 1500,
    })
    .setClassToggle('#link-' + attractionID, 'active')
  controller.addScene(attractionsMenu);
});

// Animate Blur
// var headerBlur = new ScrollMagic.Scene({
//   triggerElement: '.single-attraction .text-side',
//   duration: 400
// })
// headerBlur.setTween('.background-image', 0.5, {'filter':'blur("10px")'})
// controller.addScene(headerBlur);

// Parallax Scrolling
$(".parallax-up-heavy").each(function() {
	var upHeavy = new TimelineMax();
	upHeavy.to(this, 1, { y: -400, ease: Linear.easeNone });

	var scene = new ScrollMagic.Scene({
		triggerElement: this,
		triggerHook: 1,
		duration: "100%"
	})
		.setTween(upHeavy)
		.addTo(controller);
	});

	$(".parallax-up-light").each(function() {
	var upLight = new TimelineMax();
	upLight.to(this, 1, { y: -200, ease: Linear.easeNone });

	var scene = new ScrollMagic.Scene({
		triggerElement: this,
		triggerHook: 1,
		duration: "100%"
	})
		.setTween(upLight)
		.addTo(controller);
	});