var videoSlider = $('.video-slider').slick({
  fade: true,
  speed: 3000,
  autoplay: true,
  autoplaySpeed: 5000,
  arrows: false,
  draggable: false,
  dots: true,
  lazyLoad: 'progressive',
});
