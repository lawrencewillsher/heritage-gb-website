import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/slick';
import './lib/sliders';
import './lib/scroll-magic';
import './lib/magic-config';
import './lib/smooth-scroll-polyfills';
import './lib/custom-scripts';

$(document).foundation();
