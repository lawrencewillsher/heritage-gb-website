<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div id="post_content" class="post-content black">
		<article class="main-container ">
			<div class="content-container">
				<header>
					<h1><?php the_title(); ?></h1>
					<p><?php the_time( get_option( 'date_format' ) ); ?></p>
					<p><?php if ( get_field('reading_time') ) : echo get_field('reading_time'); endif; ?></p>
				</header> 

				<?php the_content(); ?>

				<footer>
					<a href="#" id="share" class="circle-button">Share</a>
				</footer>
				
			</div>
		</article>
	</div>
<?php endwhile; ?>

<?php get_footer();
