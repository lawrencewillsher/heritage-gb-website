<div id="loading-cover">
    <div class="loading-logo">
        <?php the_custom_logo(); ?>
    </div>

    <div class="loader-text">
        <?php if ( get_field('loader_text', 'options') ) : ?>
            <h1><?php echo get_field('loader_text', 'options'); ?></h1>
        <?php endif; ?>
    </div>

    <div id="loader">
        <svg id="animated" viewbox="0 0 100 100">
            <circle cx="50" cy="50" r="45" fill="none"/>
            <path id="progress" stroke-linecap="round" stroke-width="5" stroke="#fff" fill="none"
                        d="M50 10
                            a 40 40 0 0 1 0 80
                            a 40 40 0 0 1 0 -80">
            </path>
            <text id="count" x="50" y="50" text-anchor="middle" dy="7" font-size="20" fill="#fff">100%</text>
        </svg>
        <script>
            var count = $(('#count'));
            $({ Counter: 0 }).animate({ Counter: count.text() }, {
            duration: 3200,
            easing: 'linear',
            step: function () {
                count.text(Math.ceil(this.Counter)+ "%");
            }
            });
            var s = Snap('#animated');
            var progress = s.select('#progress');
            progress.attr({strokeDasharray: '0, 251.2'});
            Snap.animate(0,251.2, function( value ) {
                    progress.attr({ 'stroke-dasharray':value+',251.2'});
            }, 3200);
        </script>
    </div>
</div>