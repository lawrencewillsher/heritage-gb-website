<?php
  $args = array(
    'post_type'=>'attraction',
		'posts_per_page' => -1, 
    'order'=> 'ASC'
  );
  $attractions = new WP_Query($args);
  if($attractions->have_posts()) : 
?>
<nav class="attraction-menu">
  <!-- <i class="far fa-circle top"></i> -->
  <ul>
  <?php 
    while($attractions->have_posts()) : $attractions->the_post(); 
    $link_id = $attractions->post->post_name;
  ?>
    <li><a id="link-<?php echo $link_id; ?>" href="#<?php echo $link_id; ?>"><span class="indicator"><i class="fas fa-circle"></i></span><span class="title"><?php the_title();?></span></a></li>
  <?php endwhile; ?>
  </ul>
  <!-- <i class="far fa-circle bottom"></i> -->
</nav>
<?php 
  wp_reset_query();
  endif; 
?>