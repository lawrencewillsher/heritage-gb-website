<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
	<header class="featured-hero" role="banner" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	
		<?php if ( is_single() && 'post' == get_post_type() ) { ?>
			<div class="left-link">
				<?php next_post_link('%link', 'Previous <span class="indicator"><i class="fas fa-circle"></i></span>', TRUE); ?>
			</div>

			<div class="right-link">
				<?php previous_post_link('%link', '<span class="indicator"><i class="fas fa-circle"></i></span> Next', TRUE); ?>
			</div>
			
		<?php } else { ?>
		
				<div class="left-link">
					<a href="javascript:history.back()"><span class="indicator"><i class="fas fa-circle"></i></span> Back</a>
				</div>

				<div class="right-link">
					<a href="/news">News <span class="indicator"><i class="fas fa-circle"></i></span></a>
				</div>

		<?php } ?>



		<div class="scroll-indicator">
			<a href="#section-anchor" class="indicator">
				<i class="fas fa-circle"><i class="fas fa-circle"></i></i>
			</a>
			<p class="scroll-text">Scroll</p>
		</div>

		<div class="image-cover"></div>
		<div class="overlay">
			<div class="attractions-text">
				<div class="inner-text">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</header>
<?php endif; ?>