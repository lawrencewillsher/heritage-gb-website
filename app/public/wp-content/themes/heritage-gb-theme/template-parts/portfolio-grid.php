<div class="portfolio" id="portfolio">
  <?php
    $args = array(
      'post_type'=>'attraction',
      'posts_per_page' => -1, 
      'order'=> 'ASC'
    );
    $attraction_query = new WP_Query($args);
    if($attraction_query->have_posts()) : 
  ?>
  <div class="full-section no-flex">
    <div class="section-title">
      <h2>Our Portfolio</h2>
    </div>

    <nav class="portfolio-nav align-center-middle">
      <ul class="portfolio-grid">
      <?php 
        while($attraction_query->have_posts()) : $attraction_query->the_post(); 
        $aimage = get_field('attraction_logo');
      ?>
        <li><a href="<?php the_permalink(); ?>"><img src="<?php echo $aimage['url']; ?>" alt="<?php echo $aimage['alt']; ?>"></a></li>
      <?php endwhile; ?>
      </ul>
    </nav>
  </div>
  <?php 
  wp_reset_query();
  endif; ?>

  <div class="scroll-indicator">
      <a href="#lands-end" class="indicator">
        <i class="fas fa-circle"></i>
      </a>
      <p class="scroll-text">See More</p>
    </div>
</div>

