<?php
  $args = array(
    'post_type'=>'attraction',
		'posts_per_page' => -1
  );
  $attractions = new WP_Query($args);
  if($attractions->have_posts()) : 
?>
<div class="attractions-list">
  <?php 
    $a=0;
    while($attractions->have_posts()) : $attractions->the_post(); 
    $a++;
    $attraction_bg = get_the_post_thumbnail_url();
    $link_id = $attractions->post->post_name;
    $next_post = get_next_post();
    $next_post_slug = $next_post->post_name;
    //print_r($next_post_slug);
  ?>
    <div id="<?php echo $link_id; ?>"></div>
    
    <div class="attraction-home trigger full-section" >
  
      <div class="image" style="background-image: url(<?php echo $attraction_bg; ?>);">
        <div class="image-cover"></div>
      </div>

      

      <div class="scroll-indicator">
        <a href="#<?php echo $next_post_slug; ?>" class="indicator">
          <i class="fas fa-circle"></i>
        </a>
        <p class="scroll-text"><?php echo get_the_title($next_post); ?></p>
      </div>

      <div class="attractions-text">
        <div class="inner-text">
          <h2><?php if(get_field('attraction_full_title')) { the_field('attraction_full_title'); } else { the_title(); } ?></h2>
          <p class="strapline"><?php the_field('attraction_strapline'); ?></p>
        </div>
        <a href="<?php the_permalink(); ?>" class="circle-button">View</a>
      </div>

    </div>
    <span class="trigger"></span>
  <?php endwhile; ?>
</div>
<?php 
  wp_reset_query();
  endif; 
?>