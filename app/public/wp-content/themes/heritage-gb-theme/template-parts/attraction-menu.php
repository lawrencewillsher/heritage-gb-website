<?php
  $args = array(
    'post_type'=>'attraction',
		'posts_per_page' => -1, 
    'order'=> 'ASC'
  );
  $attraction_query = new WP_Query($args);
  if($attraction_query->have_posts()) : 
?>
<nav class="attraction-menu">
  <i class="far fa-circle"></i>
  <ul>
  <?php while($attraction_query->have_posts()) : $attraction_query->the_post(); ?>
    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
  <?php endwhile; ?>
  </ul>
  <i class="far fa-circle"></i>
</nav>
<?php 
  wp_reset_query();
  endif; 
?>