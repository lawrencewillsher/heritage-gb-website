<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="mobile-off-canvas-menu off-canvas position-top" id="<?php foundationpress_mobile_menu_id(); ?>" data-off-canvas data-transition="overlap" data-auto-focus="false" role="navigation">
	<div class="main-container">
		<?php
			$args = array(
				'post_type'=>'attraction',
				'posts_per_page' => -1, 
				'order'=> 'ASC'
			);
			$attractions = new WP_Query($args);
			if($attractions->have_posts()) : 
		?>
			<ul class="menu-logos">
				<?php 
					while($attractions->have_posts()) : $attractions->the_post(); 
					$aimage = get_field('attraction_logo');
				?>
					<li class="flex-container align-center-middle"><a href="<?php the_permalink(); ?>"><img src="<?php echo $aimage['url']; ?>" alt="<?php echo $aimage['alt']; ?>"></a></li>
				<?php endwhile; ?>
			</ul>
		<?php 
		wp_reset_query();
		endif; 
		?>

		<?php foundationpress_mobile_nav(); ?>

		<div class="socialsMenu">
			<a target="_blank" href="https://www.facebook.com/HeritageGB">Facebook</a>
			<a target="_blank" href="https://twitter.com/HeritageGB">Twitter</a>
			<a target="_blank" href="https://www.linkedin.com/company/heritage-great-britain-plc">Linkedin</a>
		</div>

	</div>

	<div class="menu-bar-bottom">
		<a href="/privacy-policy" class="left">Legal Pages</a>
	</div>
</nav>

<div class="off-canvas-content" data-off-canvas-content>
