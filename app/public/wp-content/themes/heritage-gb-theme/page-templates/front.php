<?php
/*
Template Name: Front
*/
get_header(); ?>

<?php get_template_part( 'template-parts/attraction-menu-home' ); ?>

<header class="front-hero bg-video" role="banner">
	<?php if ( have_rows('video_slider') ) : ?>
		<div class="video-slider">
			<?php while( have_rows('video_slider') ) : the_row(); ?>
				<div class="slide">
					<video src="<?php the_sub_field('video'); ?>" autoplay muted loop poster="<?php the_sub_field('image'); ?>">
						<source data-lazy="<?php the_sub_field('video'); ?>" type="video/mov">
					</video>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>

	<div class="header-text">
		<h1><?php if(get_field('main_heading')) { the_field('main_heading'); } else { the_title(); } ?></h1>
		<?php if(get_field('sub_heading')) { ?>
			<p class="lead"><?php the_field('sub_heading'); ?></p>
		<?php } ?>
		<!-- <span class="indicator"><i class="fas fa-circle"></i></span> -->
	</div>

	<div class="right-link">
		<a href="/news/">News <span class="indicator"><i class="fas fa-circle"></i></span></a>
	</div>
	
	<div class="scroll-indicator">
		<p>See our portfolio</p>
		<a href="#portfolio" class="indicator">
			<i class="fas fa-circle"></i>
		</a>
		<p class="scroll-text">Scroll</p>
	</div>
</header>

<?php get_template_part( 'template-parts/portfolio-grid' ); ?>

<?php do_action( 'foundationpress_before_content' ); ?>

<?php get_template_part( 'template-parts/attractions-home' ); ?>

<?php do_action( 'foundationpress_after_content' ); ?>

<?php get_footer();
