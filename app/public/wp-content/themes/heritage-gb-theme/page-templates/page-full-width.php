<?php
/*
Template Name: Full Width
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>
<div class="full-bg blue">
	<div class="main-container">
		<div class="main-grid">
			<main class="main-content-full-width thin">
				<?php while ( have_posts() ) : the_post(); ?>
					<h1><?php echo the_field('page_subtitle'); ?></h1>
					<?php the_content(); ?>
				<?php endwhile; ?>

				<?php if ( have_rows('download_list') ) : ?>
					<?php while( have_rows('download_list') ) : the_row();
						//vars
						$title = get_sub_field('title');
						$subtitle = get_sub_field('subtitle');
					?>
						<p><strong><?php echo $title; ?></strong></p>
						<p><?php echo $subtitle; ?></p>

						<?php if ( have_rows('download_button') ) : ?>
							<?php while( have_rows('download_button') ) : the_row();
								//vars
								$text = get_sub_field('text');
								$file = get_sub_field('file');
							?>
								<a href="<?php echo $file['url']; ?>" class="circle-button" target="_blank"><?php echo $text; ?></a>
							<?php endwhile; ?>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</main>
		</div>
	</div>
</div>
<div class="gradient-matcher"></div>

<?php get_footer();