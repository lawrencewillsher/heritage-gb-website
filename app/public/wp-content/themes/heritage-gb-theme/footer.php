<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<!-- Contact anchor -->
<div id="contact"></div>

<footer class="site-footer">

	<?php if ( is_singular( 'attraction' ) ) { ?>

		<div class="footer-grid">
			<section id="text-2" class="widget widget_text">			
				<div class="textwidget">
					<h2>Discover more</h2>
					<h3>If you want to find out more about this or any of our other landmark attractions, get in touch using the information below or continue exploring <a class="line-link" href="/#portfolio">Our Portfolio</a></h3>
					<p>5th floor,<br>
					88 Church Street,<br>
					Liverpool,<br>
					L1 3HD.</p>
					<p><a href="tel:01517060077">0151 706 0077</a><br>
					<a href="mailto:info@heritagegb.co.uk">info@heritagegb.co.uk</a></p>
					<p>Registered in England: No 2808359</p>
					<br>
					<a href="/privacy-policy" class="left">Legal Pages</a>
					<div class="footer-links">
						<a class="circle-button news" href="/news">News</a>
						<a class="circle-button facebook" target="_blank" href="https://www.facebook.com/HeritageGB">Facebook</a>
						<a class="circle-button twitter" target="_blank" href="https://twitter.com/HeritageGB">Twitter</a>
						<a class="circle-button linkedin" target="_blank" href="https://www.linkedin.com/company/heritage-great-britain-plc">Linkedin</a>
					</div>
				</div>
			</section>
			<section id="text-3" class="widget widget_text">			
				<div class="textwidget">
					<p>
						<a href="https://www.mayoralclub.co.uk/" target="_blank" rel="noopener noreferrer"><br>
						<img style="max-height:80px;margin: 0 10px;" alt="100 club" data-src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/100club.png&quot;" class=" lazyloaded" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/100club.png&quot;"><noscript><img style="max-height:80px;margin: 0 10px;" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/100club.png&quot;" alt="100 club" /></noscript>
						</a>
						<a href="https://www.balppa.org/" target="_blank" rel="noopener noreferrer">
							<img style="max-height:80px;margin: 0 10px;" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/balppa-logo.png" alt="BALPPA" />
						</a>
					</p>
				</div>
			</section>		
			
			<a href="https://www.holdens.agency/" target="_blank" class="footer-credit">Website by <img src="https://www.holdens.agency/footer-credit/holdens-logo-white-h.svg" style="position: relative; top: 1px; vertical-align: baseline;">
		</a>
		</div>

	<?php } else { ?>

		<div class="footer-grid">
			<section id="text-2" class="widget widget_text">			
				<div class="textwidget">
					<h2>Discover more</h2>
					<h3>If you want to find out more about any of our landmark attractions, get in touch using the information below or explore more of <a class="line-link" href="/#portfolio">Our Portfolio</a></h3>
					<p>5th floor,<br>
					88 Church Street,<br>
					Liverpool,<br>
					L1 3HD.</p>
					<p><a href="tel:01517060077">0151 706 0077</a><br>
					<a href="mailto:info@heritagegb.co.uk">info@heritagegb.co.uk</a></p>
					<p>Registered in England: No 2808359</p>
					<br>
					<a href="/privacy-policy" class="left">Legal Pages</a>
					<div class="footer-links">
						<a class="circle-button news" href="/news">News</a>
						<a class="circle-button facebook" target="_blank" href="https://www.facebook.com/HeritageGB">Facebook</a>
						<a class="circle-button twitter" target="_blank" href="https://twitter.com/HeritageGB">Twitter</a>
						<a class="circle-button linkedin" target="_blank" href="https://www.linkedin.com/company/heritage-great-britain-plc">Linkedin</a>
					</div>
				</div>
			</section>
			<section id="text-3" class="widget widget_text">			
				<div class="textwidget">
					<p>
						<a href="https://www.mayoralclub.co.uk/" target="_blank" rel="noopener noreferrer"><br>
						<img style="max-height:80px;margin: 0 10px;" alt="100 club" data-src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/100club.png&quot;" class=" lazyloaded" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/100club.png&quot;"><noscript><img style="max-height:80px;margin: 0 10px;" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/100club.png&quot;" alt="100 club" /></noscript>
						</a>

						<a href="https://www.balppa.org/" target="_blank" rel="noopener noreferrer">

						<img style="max-height:80px;margin: 0 10px;" alt="BALPPA" data-src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/balppa-logo.png" class=" lazyloaded" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/balppa-logo.png"><noscript><img style="max-height:80px;margin: 0 10px;" src="http://heritagegb.andholdens.agency/wp-content/uploads/2020/01/balppa-logo.png" alt="BALPPA" /></noscript>
						</a>
					</p>
				</div>
			</section>		
			
			<a href="https://www.holdens.agency/" target="_blank" class="footer-credit">Website by 
			<img style="position: relative; top: 1px; vertical-align: baseline;" src="https://www.holdens.agency/footer-credit/holdens-logo-white-h.svg" style="position: relative; top: 1px; vertical-align: baseline;"></a>
		</div>


	<?php } ?>

</footer>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>
