module.exports = {
  externals: {
    jquery: 'jQuery'
  },
  plugins: [ new webpack.ProvidePlugin({ ScrollMagic:'scrollmagic', }) ],
};
