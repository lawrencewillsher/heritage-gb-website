<?php
/**
 * The template for displaying single attractions
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php // get_template_part( 'template-parts/attraction-menu-home' ); ?>

<script>
	var controller = new ScrollMagic.Controller();
</script>

<?php while ( have_posts() ) : the_post(); ?>
	<?php if ( has_post_thumbnail( $post->ID ) ) : ?>
		<div class="background-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
		
		<div class="background">
			<div class="left-link">
				<a href="/"><span class="indicator"><i class="fas fa-circle"></i></span> Back</a>
			</div>

			<div class="right-link">
				<a href="/news">News <span class="indicator"><i class="fas fa-circle"></i></span></a>
			</div>
			<div class="right-link-website">
				<a class="website" target="_blank" href="<?php the_field('attraction_website'); ?>">Visit <span class="indicator"><i class="fas fa-circle"></i></span></a>
			</div>
			<div class="scroll-indicator">
				<a href="#section-anchor" class="indicator">
					<i class="fas fa-circle"></i>
				</a>
				<p class="scroll-text">Scroll</p>
			</div>

			<div class="image-cover"></div>
			<div class="overlay">
				<div class="attractions-text">
					<div class="inner-text">
						<h1><?php if(get_field('attraction_full_title')) { the_field('attraction_full_title'); } else { the_title(); } ?></h1>
						<p class="strapline"><?php the_field('attraction_strapline'); ?></p>
					</div>
				</div>
			</div>

			<?php if ( have_rows('attraction_images') ) : ?>
				<div id="section-anchor"></div>
				<?php while( have_rows('attraction_images') ) : the_row(); 
					//vars
					$image = get_sub_field('image');
					$parallaxType = get_sub_field('parallax_type');
					$level = get_sub_field('image_level');
					$imagePosition = get_sub_field('image_position');
					$title = get_sub_field('title');
					$titlePosition = get_sub_field('title_position');
				?>

					<?php if( $imagePosition == 'full' || $imagePosition == 'full-margin-top' || $imagePosition == 'full-margin-bottom' || $imagePosition == 'full-margin-both' ) : ?>
						<div class="parallax-section <?php echo $parallaxType; ?>">
							<figure class="<?php echo $imagePosition; ?> <?php echo $level; ?>" style="background-image: url('<?php echo $image['url']; ?>');">
								<h1><?php echo $title; ?></h1>
							</figure>
						</div>
					<?php else: ?>
						<div class="parallax-section <?php echo $parallaxType; ?> contained-image">
							<figure class="<?php echo $imagePosition; ?> <?php echo $level; ?>" style="background-image: url('<?php echo $image['url']; ?>');">
							</figure>

							<?php if ( $title ) : ?>
								<div class="image-title <?php echo $titlePosition; ?>">
									<h1><?php echo $title; ?></h1>
								</div>
							<?php endif; ?>
							<div class="clear"></div>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php if ( get_field('attraction_video') ) : ?>"
				<div class="responsive-embed main-container">
					<?php echo get_field('attraction_video'); ?>
				</div>
			<?php endif; ?>
			

			<?php 
				//vars
				$image = get_field('attraction_content_background_image');	
			?>
			<section class="full-section" style="background: url('<?php echo $image['url']; ?>') no-repeat bottom center; background-size: cover;">
				<div class="image-cover darker"></div>
				<div class="attraction-content">
					<div class="text-side">
						<!-- <h1><?php // if(get_field('attraction_full_title')) { the_field('attraction_full_title'); } else { the_title(); } ?></h1>
						<h2 class="strapline"><?php // the_field('attraction_strapline'); ?></h2> -->
						<?php the_content(); ?>
					</div>
					<div class="linkWrap">
						<?php if ( get_field('attraction_logo') ) : $image = get_field('attraction_logo'); ?>
							<a target="_blank" href="<?php the_field('attraction_website'); ?>">
								<img class="attraction-logo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
							</a>
						<?php endif; ?>
						
						<a target="_blank" href="<?php the_field('attraction_website'); ?>" class="circle-button">Visit website</a>
					</div>
				</div>
			</section> 
		</div>
	<?php endif; ?>	
<?php endwhile; ?>

<?php get_footer();