<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="left-link">
	<a href="javascript:history.back()"><span class="indicator"><i class="fas fa-circle"></i></span> Back</a>
</div>

<?php if ( have_posts() ) : ?>
	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); 
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	?>
		<section class="news-post" style="background: url('<?php echo $featured_img_url; ?>') no-repeat center center; background-size: cover;">
			<div class="image-cover"></div>

			<div class="scroll-indicator">
				<a href="#section-anchor" class="indicator">
					<i class="fas fa-circle"><i class="fas fa-circle"></i></i>
				</a>
				<p class="scroll-text">Scroll</p>
			</div>
			
			<article class="main-container">
				<div class="content-container overlay">
					<header>
						<h1><?php the_title(); ?></h1>
						<p><?php the_time( get_option( 'date_format' ) ); ?></p>
					</header> 

					<?php the_excerpt(); ?>

					<footer>
						<a href="<?php the_permalink(); ?>" class="circle-button">Read more</a>
					</footer>
				</div>
			</article>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer();
