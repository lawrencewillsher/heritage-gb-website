<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z2eOVki3gTFC+QvaSp/L0YbUy2FWnux/6VgoeIk+H71ovOv2QJqA3zs93wmnwAjU0v5l3kI1YXaUne6W1OqPIA==');
define('SECURE_AUTH_KEY',  's3hcLRp18HCgCWSxolQrELjfZUnFIYlhWRzwXcgDpUIFJWjqTvCdCevjinid19bPrl9D/apCxGkJ4Fqa2VQ1tA==');
define('LOGGED_IN_KEY',    '0iGomM2Ph0fAwGKBOK8o69BAnqHBETWz09WvJNuAO8eByW6ui/DD/G8ECcSaCT8r32Okj2wEFW7IP7dgTX5tXA==');
define('NONCE_KEY',        'geaq5L0JFuFqoheg86Z/O/79Xc0fNi2m9cEqRjGOsYmp+5mYWeM3RrO0v9+27aL234NcWN/Orf4PJ/B36F1z5g==');
define('AUTH_SALT',        '4x3Tip+F08oPAg2VI5yS6dLZyUae1di7kiGyeXROR++ZcwXHRoB4htqf0K8uTg2ntJf2VVLhvPXfmr0rbgTpzg==');
define('SECURE_AUTH_SALT', 'p73RidbknDE8Bq4iAKr1eJ+Qe26sXQSJ/KBaT0UZlmLukkaJyyjwO5gkoSU+/0tjHMEY6JT2pmlaMY64WOKGlg==');
define('LOGGED_IN_SALT',   '5EpuA4CD7F7h6k65epQab3yVeAKVBglxP8yPT2QgzUh4bXqbvMmjtzpYVTDwZ5YPb7/7PG3pwJabLvOcbS3vWQ==');
define('NONCE_SALT',       'KG+OF9Nr9VY8bm3YocOattU2FS3TsTbTaknWdPYGE08oSWj+VuZrYveHXu8wLNzof6Ur2cAvea9iYfFpLbve8Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
